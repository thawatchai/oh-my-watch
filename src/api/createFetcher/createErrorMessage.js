const getMessage = (res, options) => {
  return `Service error | ${options.url}`
}

export default (res, options) => {
  throw Object({
    type: 'ERROR',
    message: getMessage(res, options)
  })
}
